---

stages:
  - test
  - registry
  - deploy

# test
yamllint:
  stage: test
  image:
    name: registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/cytopia__yamllint:latest
    entrypoint: ["/bin/ash", "-c"]
  script:
    - yamllint -c ci/config/yamllint_config.yaml .

# registry
.sshuttle:  # Required to access prometheus push gateway
  variables: &sshuttle_vars
    SSH_EXTRA_OPTS: "-o StrictHostKeyChecking=no"
    SSHUTTLE_ROUTED_SUBNETS: "10.16.0.0/16 147.251.62.0/24 147.251.63.0/24"
    SSHUTTLE_EXCLUDED_SUBNETS: "147.251.62.9/32"
  before_script: &sshuttle_before_script
    - mkdir -p "${HOME}/.ssh"
    - ssh_private_key_file="SSH_PRIVATE_KEY_PRODUCTION"
    - cp -f "${!ssh_private_key_file}" "${HOME}/.ssh/id_rsa"
    - chmod 700 "${HOME}/.ssh"
    - chmod 600 "${HOME}/.ssh/id_rsa"
    - ssh "${SSH_EXTRA_OPTS}" root@jump.cloud.muni.cz 'uname -a'
    - sshuttle --remote root@jump.cloud.muni.cz --listen 0.0.0.0 --exclude ${SSHUTTLE_EXCLUDED_SUBNETS} --daemon --ssh-cmd "ssh ${SSH_EXTRA_OPTS}" ${SSHUTTLE_ROUTED_SUBNETS}

.create-pages-source:
  script: &create-pages-source_script
    - python3 ci/src/create_registry_table_content.py --input-path image-description.yaml --output-path ci/html/middle --container-exec docker --log-level info
    - cat ci/html/top > ci/html/index.html && cat ci/html/middle >> ci/html/index.html && cat ci/html/bottom >> ci/html/index.html
  artifacts: &create-pages-source_artifacts
    paths:
      - ci/html

.update-images-template:
  stage: registry
  image: registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/docker.io__debian:bullseye
  before_script: &update-images-template_before_script
    - ci/dependencies/install-pkgs.sh ci/dependencies/requirements.apt > pkgs-install.log
    - ci/dependencies/install-pymodules.sh ci/dependencies/requirements.pip > pymodules-install.log
    - service docker start
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

.update-registry-images: &update-registry-images
  extends: .update-images-template
  variables:
    PUSHGATEWAY_URL: "http://monitoring1.priv.cloud.muni.cz:9091"
    <<: *sshuttle_vars
  before_script:
    - *update-images-template_before_script
    - *sshuttle_before_script
  script:
    # 14.11.2024 push metric fails on timeout, metric is pushed to G1 but we need this in G2 anyway, TODO: fix and reenable
    #- ./ci/src/push_metric.py --metric-label app=cloud/g2/container-registry.git --metric-name job_last_run_timestamp --metric-help "Last container registry update execution timestamp." --metric-value `date "+%s"` --pushgateway-url "${PUSHGATEWAY_URL}"
    - python3 ci/src/registry_synchronizer.py --input-path image-description.yaml --container-exec docker --log-level info
    #- ./ci/src/push_metric.py --metric-label app=cloud/g2/container-registry.git --metric-name job_last_successful_run_timestamp --metric-help "Last container registry update successful job execution timestamp." --metric-value `date "+%s"` --pushgateway-url "${PUSHGATEWAY_URL}"
    - *create-pages-source_script
  artifacts:
    <<: *create-pages-source_artifacts

update-registry-images_schedules:
  extends: .update-registry-images
  only:
    - schedules

update-registry-images_manual:
  extends: .update-registry-images
  except:
    - schedules
  when: manual

update-unstored-images:
  extends: .update-images-template
  script:
    - python3 ci/src/registry_synchronizer.py --input-path image-description.yaml --container-exec docker --only-unstored True --log-level info
    - *create-pages-source_script
  artifacts:
    <<: *create-pages-source_artifacts
  except:
    - schedules

# deploy
pages:
  stage: deploy
  image: registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/docker.io__busybox:latest
  before_script:
    - mv -f ci/html/index.html public/index.html
  script:
    - echo "The site will be deployed to $CI_PAGES_URL"
  artifacts:
    paths:
      - public
