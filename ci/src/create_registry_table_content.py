#!/usr/bin/env python3

'''
Script reads .yaml file, containing a description of container images,
checks their presence in a local container registry
and creates content of html table, listing present images.

usage: create_registry_table_content.py [-h] --input-path INPUT_PATH
                                        [--output-path OUTPUT_PATH]
                                        [--container-exec CONTAINER_EXEC]
                                        [--log-level LOG_LEVEL]

Descriptive file syntax example (in downstream, '/' is replaced with 2 of '_'):

---
downstream_container_registry:
  address: registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/

images:
  - upstream-image: docker.io/cytopia/yamllint:latest
    downstream-images:
      - docker.io__cytopia__yamllint:latest
    overwrite-existing: true

  - upstream-image: docker.io/openstackhelm/mariadb:ubuntu_focal-20210415
    downstream-images:
      - docker.io__openstackhelm__mariadb:ubuntu_focal-20210415
    overwrite-existing: true
'''

import argparse
import logging
import subprocess # Run bash commands
from ast import literal_eval # Interpret dictionary_string as dictionary
from collections import OrderedDict
import yaml # For reading descriptive file

LOG_LEVELS = {
    'debug':logging.DEBUG,
    'info':logging.INFO,
    'warning':logging.WARNING,
    'error':logging.ERROR,
    'critical':logging.CRITICAL}

class RegistryPresence:
    ''' Synchronizes local image registry with docker hub '''
    def __init__(self, args):
        self.input_path = args.input_path
        self.output_path = args.output_path
        self.container_exec = args.container_exec
        self.present_images = 0
        self.present_tags = 0
        self.presence_state = {}
        self.not_present = {}

    def print_summary(self):
        ''' Print action summary at the end of presence check '''
        logging.info("%d images present with total of %d tags.",
                     self.present_images, self.present_tags)
        if self.not_present:
            logging.info("Number of images that should be present, but aren't: %d",
                         len(self.not_present))

    def run_command(self, command):
        ''' Wrapper for running bash commands + exception handling '''
        try:
            output = subprocess.run(command, check=True, capture_output=True)
            logging.debug("%s returned: %s", command, output)
            return (output.stdout).decode('utf-8')
        except subprocess.CalledProcessError as ex:
            logging.info((ex.stderr).decode('utf-8'))
            raise

    def get_manifest(self, image):
        ''' Returns image manifest '''
        command = ["podman", "manifest", "inspect", image]
        output = subprocess.run(command, check=True, capture_output=True)
        logging.debug("Manifest inspect output: %s", output)
        return literal_eval((output.stdout).decode('utf-8'))

    def get_local_digest(self, local_image):
        ''' Check local image manifest, returns manifest or False '''
        try:
            return (self.get_manifest(local_image))['config']['digest']
        except subprocess.CalledProcessError as ex: # No manifest in local registry is not an error
            if 'no such manifest' in str(ex.stderr) or 'manifest unknown' in str(ex.stderr):
                return False

            logging.info((ex.stderr).decode('utf-8'))
            raise

    def in_local_registry(self, image):
        ''' Check whether local registry image is present '''
        local_digest = self.get_local_digest(image)
        logging.debug("Local registry image digest: %s", local_digest)
        if not local_digest:
            logging.info("Not found locally: %s", image)
            return False
        return True

    def check(self):
        ''' Read descriptive file, check existence of local digest, save to dictionary '''
        with open(self.input_path, 'r', encoding="utf-8") as desc_file:
            image_description = yaml.safe_load(desc_file)
        logging.debug("Opened descriptive file: %s", self.input_path)

        registry_url = image_description['downstream_container_registry']['address']
        logging.debug("Registry URL: %s", registry_url)

        for image in image_description['images']:
            hub_image = image['upstream-image']
            logging.debug("Working with image: %s", hub_image)

            for local_image in image['downstream-images']:
                # Add registry URL to name, if it is not part of downstream image name
                if not registry_url in local_image:
                    local_image = registry_url + local_image
                logging.debug("Downstream image: %s", local_image)

                # Check if local registry image is present and add to dictionary
                if self.in_local_registry(local_image):
                    hub_image_tagless = hub_image.split(':')[0]
                    local_image_tag = local_image.split(':')[2]

                    if not hub_image.split(':')[0] in self.presence_state:
                        self.presence_state[hub_image_tagless] = []
                        self.present_images += 1
                    self.presence_state[hub_image_tagless].append({local_image_tag: local_image})
                    self.present_tags += 1
                else:
                    self.not_present[hub_image] = local_image

    def create_html_table_content(self):
        ''' Creates html file, containing table content of present images at OUTPUT_PATH '''
        table_content = OrderedDict(sorted(self.presence_state.items()))
        with open(self.output_path, 'w', encoding="utf-8") as webpage_file:
            for hub_image, local_image_list in table_content.items():
                tags = "<td>"
                local_images = "<td>"
                for tag_local_image in local_image_list:
                    for tag, local_image in tag_local_image.items():
                        tags += f"{tag}<br><br>"
                        local_images += f"{local_image}<br>"

                webpage_file.write(f"<tr><td>{hub_image}</td>{tags}</td>{local_images}</td></tr>\n")

def parse_arguments():
    ''' Returns parsed command line arguments '''
    parser = argparse.ArgumentParser()

    parser.add_argument('--input-path', '-f', required=True, type=str,
                        help='Path to descriptive YAML file.')
    parser.add_argument('--output-path', default="registry_table_content.html", type=str,
                        help='Path to webpage html file. (default: %(default)s)')
    parser.add_argument('--container-exec', default="docker", type=str,
                        help='Software for managing containers. (default: %(default)s)')
    parser.add_argument('--log-level', default="info", type=str,
                        help='Level of displayed logs. (default: %(default)s)')

    return parser.parse_args()

def main():
    """ Main entry point """
    args = parse_arguments()
    logging.basicConfig(format='%(levelname)s: %(message)s', level=LOG_LEVELS[args.log_level])
    registry_presence = RegistryPresence(args)
    registry_presence.check()
    registry_presence.print_summary()
    registry_presence.create_html_table_content()

if __name__ == "__main__":
    main()
