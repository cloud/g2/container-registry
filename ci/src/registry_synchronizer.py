#!/usr/bin/env python3

'''
Script reads .yaml file, containing a description of container images,
and synchronizes their local versions with those on hub.

usage: registry_synchronizer.py [-h] --input-path INPUT_PATH
                                [--container-exec CONTAINER_EXEC]
                                [--only-unstored ONLY_UNSTORED]
                                [--log-level LOG_LEVEL]

Descriptive file syntax example (in downstream, '/' is replaced with 2 of '_'):

---
downstream_container_registry:
  address: registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/

images:
  - upstream-image: docker.io/cytopia/yamllint:latest
    downstream-images:
      - docker.io__cytopia__yamllint:latest
    overwrite-existing: true

  - upstream-image: docker.io/openstackhelm/mariadb:ubuntu_focal-20210415
    downstream-images:
      - docker.io__openstackhelm__mariadb:ubuntu_focal-20210415
    overwrite-existing: true
'''

import argparse
import logging
import subprocess # Run bash commands
from ast import literal_eval # Interpret dictionary_string as dictionary
import yaml # For reading descriptive file

LOG_LEVELS = {
    'debug':logging.DEBUG,
    'info':logging.INFO,
    'warning':logging.WARNING,
    'error':logging.ERROR,
    'critical':logging.CRITICAL}

class RegistrySynchronizer:
    ''' Synchronizes local image registry with docker hub '''
    def __init__(self, args):
        self.input_path = args.input_path
        self.container_exec = args.container_exec
        self.only_unstored = args.only_unstored
        self.updated_images = 0
        self.new_images = 0
        self.outdated_images = set()
        self.images_without_digest = set()

    def print_summary(self):
        ''' Print action summary at the end of synchronization '''
        truly_updated = self.updated_images - self.new_images
        logging.info("%d updated, %d newly stored", truly_updated, self.new_images)

        known_outdated = self.outdated_images - self.images_without_digest
        if known_outdated:
            logging.info("Outdated: %s", known_outdated)

    def run_command(self, command):
        ''' Wrapper for running bash commands + exception handling '''
        try:
            output = subprocess.run(command, check=True, capture_output=True)
            logging.debug("%s returned: %s", command, output)
            return (output.stdout).decode('utf-8')
        except subprocess.CalledProcessError as ex:
            logging.info((ex.stderr).decode('utf-8'))
            raise

    def update(self, image, local_image):
        ''' Pull image from remote repository, tag it and push to local repo '''
        self.run_command([self.container_exec, "pull", image])
        self.run_command([self.container_exec, "tag", image, local_image])
        self.run_command([self.container_exec, "push", local_image])

        new_digest = self.get_local_digest(local_image)
        logging.info("Updated: %s | new local digest: %s", image, new_digest)
        self.updated_images += 1

    def get_manifest(self, image):
        ''' Returns image manifest '''
        command = ["podman", "manifest", "inspect", image]
        output = subprocess.run(command, check=True, capture_output=True)
        logging.debug("Manifest inspect output: %s", output)
        return literal_eval((output.stdout).decode('utf-8'))

    def get_hub_digest(self, image):
        ''' Returns image digest, handles failures '''
        try:
            hub_manifest = self.get_manifest(image)
            return hub_manifest['config']['digest']
        except KeyError: # List of manifests was returned, image manifest is needed
            logging.debug("Received list of manifests for different architectures.")
            for manifest in hub_manifest['manifests']:
                if manifest['platform']['architecture'] == "amd64":
                    manifest_digest = manifest['digest']
                    image = image.split(':')[0] + '@' + manifest_digest
                    logging.debug("Trying amd64 version: %s", image)
                    return self.get_hub_digest(image)
            raise
        except subprocess.CalledProcessError as ex: # Can't access hub digest, but image exists
            if (('unsupported manifest media type and no default available' in str(ex.stderr) or
                 'error parsing manifest blob' in str(ex.stderr))
                and self.run_command(["skopeo", "inspect", f"docker://{image}"])):
                return False

            logging.info((ex.stderr).decode('utf-8'))
            raise

    def get_local_digest(self, local_image):
        ''' Check local image manifest, returns manifest or False '''
        try:
            return (self.get_manifest(local_image))['config']['digest']
        except subprocess.CalledProcessError as ex: # No manifest in local registry is not an error
            if 'no such manifest' in str(ex.stderr) or 'manifest unknown' in str(ex.stderr):
                return False

            logging.info((ex.stderr).decode('utf-8'))
            raise

    def up_to_date(self, image, local_image):
        ''' Compare digests of local registry image and docker hub image '''
        local_digest = self.get_local_digest(local_image)
        logging.debug("Local registry image digest: %s", local_digest)
        if not local_digest:
            logging.info("Not found locally: %s", image)
            self.update(image,local_image)
            self.new_images += 1
            return True

        if self.only_unstored:
            return True

        hub_digest = self.get_hub_digest(image)
        logging.debug("Hub image digest: %s", hub_digest)
        if not hub_digest:
            logging.info("Unsupported hub manifest type, but image exists locally: %s", image)
            self.images_without_digest.add(image)
            return False

        if local_digest == hub_digest:
            logging.info("Up to date: %s | digest: %s", image, hub_digest)
            return True

        logging.info("Outdated image: %s | hub digest: %s, local digest: %s",
                     image, hub_digest, local_digest)
        return False

    def synchronize(self):
        ''' Read descriptive file, check local and hub digests, update outdated '''
        with open(self.input_path, 'r', encoding="utf-8") as desc_file:
            image_description = yaml.safe_load(desc_file)
        logging.debug("Opened descriptive file: %s", self.input_path)

        registry_url = image_description['downstream_container_registry']['address']
        logging.debug("Registry URL: %s", registry_url)

        for image in image_description['images']:
            hub_image = image['upstream-image']
            logging.debug("Working with image: %s", hub_image)

            for local_image in image['downstream-images']:
                # Add registry URL to name, if it is not part of downstream image name
                if not registry_url in local_image:
                    local_image = registry_url + local_image
                logging.debug("Downstream image: %s", local_image)

                # Check if registry image is up to date with hub image
                if not self.up_to_date(hub_image,local_image):
                    if image['overwrite-existing']:
                        self.update(hub_image,local_image)
                    else:
                        self.outdated_images.add(hub_image)

        self.print_summary()

def parse_arguments():
    ''' Returns parsed command line arguments '''
    parser = argparse.ArgumentParser()

    parser.add_argument('--input-path', '-f', required=True, type=str,
                        help='Path to descriptive YAML file.')
    parser.add_argument('--container-exec', default="docker", type=str,
                        help='Software for managing containers. (default: %(default)s)')
    parser.add_argument('--only-unstored', default=False, type=bool,
                        help='Just download unstored (new) images. (default: %(default)s)')
    parser.add_argument('--log-level', default="info", type=str,
                        help='Level of displayed logs. (default: %(default)s)')

    return parser.parse_args()

def main():
    """ Main entry point """
    args = parse_arguments()
    logging.basicConfig(format='%(levelname)s: %(message)s', level=LOG_LEVELS[args.log_level])
    registry_synchronizer = RegistrySynchronizer(args)
    registry_synchronizer.synchronize()

if __name__ == "__main__":
    main()
