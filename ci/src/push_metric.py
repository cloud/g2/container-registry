#!/usr/bin/env python3

"""
push-metric.py prepares and pushes customized metrics into Prometheus Pushgateway


Usage:
python3 push-metric.py  --metric-type ["Gauge","Histogram","Counter"]
                        --metric-label [job=<job_name>|app=<app_name>]
                          (- Either job or app name label must be specified)
                        --metric-label <label_name>=<label_description>
                        --pushgateway-url <pushgateway_url>
                        --metric-name <metric_name>
                        --metric-help <metric_help>
                        --metric-value <metric_value>

Example of usage:
 * Push metric job_last_run_timestamp{app="image-rotation"} <timestamp>
   into Prometheus Pushgateway at pgw.stage
   * $ ./push-metric.py    --metric-label job=image-rotation
                           --pushgateway-url 'http://monitoring1.stage.priv.cloud.muni.cz:9091'
                             (- port must be specified)
                           --metric-name job_last_successful_run_timestamp
                           --metric-help 'Last image rotation successful job execution timestamp.'
                           --metric-type Gauge
                           --metric-value `date "+%s"`

   * This command executes script for job named "image-rotation".
     It creates Gauge metric named "job_last_successful_run_timestamp"/
   * Then it assigns timestamp value and help metric description
     and it pushes metric to pushgateway-url http://monitoring1.stage.priv.cloud.muni.cz:9091
"""

import argparse
import copy

import prometheus_client

class ArgKeyValuePair(argparse.Action):
    """parse `<key>=<value>` argument value string into dict"""
    def __call__(self, _, namespace, values, option_string=None):
        items = copy.copy(getattr(namespace, self.dest, {}))
        option_name, option_value = values.split("=", 1)
        items[option_name.strip()] = option_value
        setattr(namespace, self.dest, items)

def push_metric(gateway_url, job, registry):
    """Push of prepared metrics via Pushgateway"""
    prometheus_client.pushadd_to_gateway(gateway_url, job=job, registry=registry)

def prepare_metric(args, registry):
    """Processing and preparation of metric"""
    metric_type = getattr(prometheus_client, getattr(args, "metric_type"))
    metric = metric_type(
        args.metric_name,
        args.metric_help,
        args.metric_labels.keys(),
        registry=registry,
    )
    metric.labels(**args.metric_labels).set(args.metric_value)

def get_job_name(args):
    """Get job name"""
    if "job" in args.metric_labels:
        return args.metric_labels["job"]
    if "app" in args.metric_labels:
        return args.metric_labels["app"]
    return None

if __name__ == "__main__":
    AP = argparse.ArgumentParser(epilog=globals().get("__doc__"))

    AP.add_argument(
        "--metric-type",
        choices=["Gauge", "Histogram", "Counter"],
        default="Gauge",
        dest="metric_type",
        help='Metric type to push"',
    )
    AP.add_argument(
        "--metric-name",
        required=True,
        dest="metric_name",
        help='Metric name"',
    )
    AP.add_argument(
        "--metric-help",
        required=True,
        dest="metric_help",
        help='Metric help description"',
    )
    AP.add_argument(
        "--metric-value",
        required=True,
        type=float,
        dest="metric_value",
        help='Metric value to push"',
    )
    AP.add_argument(
        "--metric-label",
        default={},
        action=ArgKeyValuePair,
        required=True,
        dest="metric_labels",
        help='Metric label and its description"<label_name>=<label_description>"',
    )
    AP.add_argument(
        "--pushgateway-url",
        required=True,
        action="store",
        dest="pushgateway_url",
        help="Url for metric push",
    )
    ARGS = AP.parse_args()
    JOB = get_job_name(ARGS)
    assert (
        JOB
    ), "Either --metric-label job=<job_name> or --metric-label name=<app_name> is required"

    REGISTRY = prometheus_client.CollectorRegistry()
    prepare_metric(ARGS, REGISTRY)

    GATEWAY_URL = ARGS.pushgateway_url
    push_metric(GATEWAY_URL, JOB, REGISTRY)
