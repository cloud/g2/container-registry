# Container Registry

## Description
The description of Docker container images used in G2 OpenStack is written in ```image-description.yaml```. Images are stored locally and updated weekly.

## Usage
The image might already be stored. Check [Gitlab GUI](https://gitlab.ics.muni.cz/cloud/g2/container-registry/container_registry) or [custom webpage](https://cloud.gitlab-pages.ics.muni.cz/g2/container-registry).

Add a new image description to ```image-description.yaml```, keeping the same syntax. It is recommended to add new images to the top; they will be accessible sooner. Please, refer to ```Notes```.

Commit straight to master. Automatic pipeline will take care of the rest. Just check that you didn't break it when it finishes.

## Notes

Because gitlab doesn't allow more than 1 ```/``` in registry names, it is replaced with 2 of ```_```. Refer to the example below.

If you want to update the image with every pipeline run (weekly), use ```overwrite-existing: true```. This is recommended.

Example:
```
  - upstream-image: docker.itera.io/openstackhelm/mariadb:ubuntu_xenial-20200303
    downstream-images:
      - docker.itera.io__openstackhelm__mariadb:ubuntu_xenial-20200303
    overwrite-existing: true
```
